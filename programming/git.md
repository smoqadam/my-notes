# Git

**Stash list**  
`git stash list`  
**output:** 

`stash@{0}: ...  
stash@{1}: ...  
stash@{2}: ...`

**Stash only one file**

`git stash show stash@{1}:<filename> > <newfilename>`

\`\`

