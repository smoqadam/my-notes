# Table of contents

* [Introduction](README.md)
* [Articles](articles.md)

## Books

* [The Pragmatic Programmer](books/the-pragmatic-programmer.md)
* [Understanding Composition Field Guide](books/understanding-composition-field-guide.md)

## Programming

* [Git](programming/git.md)
* [Raspberry Pi](programming/raspberry-pi.md)

